mod models;
mod util;

use crate::models::meta;
use crate::util::json::PathedError;
use models::charm::*;
use models::charmset::Charmset;
use std::convert::TryFrom;
use std::fs::File;
use std::io::{BufReader, BufWriter, Write};
use std::path::{Path, PathBuf};

fn parse_input_json(path: PathBuf) -> Result<serde_json::Value, String> {
    match File::open(path) {
        Ok(file) => {
            let reader = BufReader::new(file);
            serde_json::from_reader(reader).map_err(|e| e.to_string())
        }
        Err(e) => Err(e.to_string()),
    }
}

fn generate_markdown(input_json: serde_json::Value) -> Result<String, PathedError> {
    match meta::Kind::try_from(&input_json)? {
        meta::Kind::Charm => Charm::try_from(&input_json).map(|c| c.to_string()),
        meta::Kind::Charmset => Charmset::try_from(&input_json).map(|cs| cs.to_string()),
    }
}

fn main() {
    let args = clap::App::new("Reference-Solidifying Discipline")
        .arg(
            clap::Arg::with_name("input")
                .short("i")
                .long("input")
                .value_name("INPUT_FILE")
                .required(true)
                .validator(|i| {
                    let input_file = Path::new(&i);
                    if input_file.exists() {
                        if input_file.is_file() {
                            Ok(())
                        } else {
                            Err(format!("Not a regular file: {}", i))
                        }
                    } else {
                        Err(format!("Path does not exist: {}", i))
                    }
                }),
        )
        .arg(
            clap::Arg::with_name("output")
                .short("o")
                .long("output")
                .value_name("OUTPUT_FILE")
                .required(true)
                .validator(|i| {
                    let aligned_path = if Path::new(&i).is_relative() {
                        format!(".{}{}", std::path::MAIN_SEPARATOR, i)
                    } else {
                        i
                    };
                    if let Some(parent_dir) = Path::new(&aligned_path).parent() {
                        if parent_dir.exists() {
                            if parent_dir.is_dir() {
                                Ok(())
                            } else {
                                Err(format!("Not a directory: {}", parent_dir.display()))
                            }
                        } else {
                            Err(format!("Path does not exist: {}", parent_dir.display()))
                        }
                    } else {
                        Err(format!("Could not derive parent path: {}", aligned_path))
                    }
                }),
        )
        .get_matches();

    let input_file = Path::new(args.value_of("input").unwrap());
    if let Ok(input_file_json) = parse_input_json(input_file.to_path_buf()) {
        match generate_markdown(input_file_json) {
            Ok(markdown) => {
                let output_file = Path::new(args.value_of("output").unwrap());
                let mut writer = BufWriter::new(File::create(output_file).unwrap());
                writer.write_all(markdown.as_bytes()).unwrap_or_else(|e| {
                    eprintln!("Write to {} failed: {}", output_file.display(), e);
                    std::process::abort()
                });
            }
            Err(e) => {
                eprintln!("Could not generate markdown from source: {}", e);
                std::process::abort()
            }
        }
    } else {
        eprintln!("Could not read JSON Object from {}", input_file.display());
        std::process::abort()
    }
}
