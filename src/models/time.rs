use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub enum TimeUnit {
    // Real Time
    Second,
    Minute,
    Hour,
    Day,
    Week,
    Month,
    Season,
    Year,
    Decade,
    Century,
    Millennium,

    // Narrative Time
    Instant,
    Action,
    Tick,
    Turn,
    Round,
    Feat,
    Combat,
    Scene,
    Session,
    Story,
    Chronicle,
}

impl TimeUnit {
    pub fn display_plural(&self) -> impl Display + '_ {
        DisplayPlural(self)
    }
}

impl Display for TimeUnit {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TimeUnit::Second => "second",
                TimeUnit::Minute => "minute",
                TimeUnit::Hour => "hour",
                TimeUnit::Day => "day",
                TimeUnit::Week => "week",
                TimeUnit::Month => "month",
                TimeUnit::Season => "season",
                TimeUnit::Year => "year",
                TimeUnit::Decade => "decade",
                TimeUnit::Century => "century",
                TimeUnit::Millennium => "millenium",
                TimeUnit::Instant => "instant",
                TimeUnit::Action => "action",
                TimeUnit::Tick => "tick",
                TimeUnit::Turn => "turn",
                TimeUnit::Round => "round",
                TimeUnit::Feat => "feat",
                TimeUnit::Scene => "scene",
                TimeUnit::Combat => "combat",
                TimeUnit::Session => "session",
                TimeUnit::Story => "story",
                TimeUnit::Chronicle => "chronicle",
            }
        )
    }
}

impl TryFrom<&String> for TimeUnit {
    type Error = String;

    fn try_from(value: &String) -> Result<Self, Self::Error> {
        match value.as_str() {
            "second" => Ok(TimeUnit::Second),
            "minute" => Ok(TimeUnit::Minute),
            "hour" => Ok(TimeUnit::Hour),
            "day" => Ok(TimeUnit::Day),
            "week" => Ok(TimeUnit::Week),
            "month" => Ok(TimeUnit::Month),
            "season" => Ok(TimeUnit::Season),
            "year" => Ok(TimeUnit::Year),
            "decade" => Ok(TimeUnit::Decade),
            "century" => Ok(TimeUnit::Century),
            "millenium" => Ok(TimeUnit::Millennium),
            "instant" => Ok(TimeUnit::Instant),
            "action" => Ok(TimeUnit::Action),
            "tick" => Ok(TimeUnit::Tick),
            "turn" => Ok(TimeUnit::Turn),
            "round" => Ok(TimeUnit::Round),
            "feat" => Ok(TimeUnit::Feat),
            "combat" => Ok(TimeUnit::Combat),
            "scene" => Ok(TimeUnit::Scene),
            "session" => Ok(TimeUnit::Session),
            "story" => Ok(TimeUnit::Story),
            "chronicle" => Ok(TimeUnit::Chronicle),
            _ => Err(format!("{} is not a valid time unit", value)),
        }
    }
}

pub struct DisplayPlural<'a>(&'a TimeUnit);

impl Display for DisplayPlural<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self.0 {
                TimeUnit::Second => "seconds",
                TimeUnit::Minute => "minutes",
                TimeUnit::Hour => "hours",
                TimeUnit::Day => "days",
                TimeUnit::Week => "weeks",
                TimeUnit::Month => "months",
                TimeUnit::Season => "seasons",
                TimeUnit::Year => "years",
                TimeUnit::Decade => "decades",
                TimeUnit::Century => "centuries",
                TimeUnit::Millennium => "millenia",
                TimeUnit::Instant => "instants",
                TimeUnit::Action => "actions",
                TimeUnit::Tick => "ticks",
                TimeUnit::Turn => "turns",
                TimeUnit::Round => "rounds",
                TimeUnit::Feat => "feats",
                TimeUnit::Combat => "combats",
                TimeUnit::Scene => "scenes",
                TimeUnit::Session => "sessions",
                TimeUnit::Story => "stories",
                TimeUnit::Chronicle => "chronicles",
            }
        )
    }
}
