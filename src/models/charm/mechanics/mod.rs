use crate::util::json;
use json::PathedError;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

mod enhance;
use enhance::Enhance;
mod special_activation;
use special_activation::SpecialActivation;

pub struct Mechanics {
    pub free_text: String,
    pub enhance: Option<Vec<Enhance>>,
    pub special_activation: Option<SpecialActivation>,
}

impl Display for Mechanics {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.free_text)?;
        if let Some(enhancements) = &self.enhance {
            for e in enhancements {
                write!(f, "\n\n{}", e)?;
            }
        }
        if let Some(sa) = &self.special_activation {
            write!(f, "\n\n{}", sa)?;
        }
        Ok(())
    }
}

impl TryFrom<&serde_json::Map<String, serde_json::Value>> for Mechanics {
    type Error = PathedError;

    fn try_from(value: &serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        Ok(Mechanics {
            free_text: json::get_string(value, "free-text")?,
            enhance: json::transform_optional_field(
                value,
                "enhance",
                Box::new(|v| {
                    json::map_array(
                        Vec::try_from(json::ValueConverter(&v))?,
                        Box::new(|inner_value| {
                            Enhance::try_from(&serde_json::Map::try_from(json::ValueConverter(
                                &inner_value,
                            ))?)
                        }),
                    )
                }),
            )?,
            special_activation: json::transform_optional_field(
                value,
                "special-activation",
                Box::new(|v| {
                    SpecialActivation::try_from(&serde_json::Map::try_from(json::ValueConverter(
                        &v,
                    ))?)
                }),
            )?,
        })
    }
}
