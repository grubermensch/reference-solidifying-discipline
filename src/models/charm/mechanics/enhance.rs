use crate::models::charm::mechanics::Mechanics;
use crate::models::charm::minimums::Minimums;
use crate::util::json;
use json::PathedError;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub enum Enhance {
    Repurchase(Option<Minimums>, Box<Mechanics>),
}

impl Display for Enhance {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Enhance::Repurchase(mins, mechanics) => {
                match mins {
                    None => write!(f, "With a repurchase: "),
                    Some(mins) => write!(f, "With a repurchase at {}: ", mins),
                }?;
                write!(f, "{}", mechanics)
            }
        }
    }
}

impl TryFrom<&serde_json::Map<String, serde_json::Value>> for Enhance {
    type Error = PathedError;

    fn try_from(value: &serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        let (key, inner_value) = json::get_singular(value)?;
        match key.as_str() {
            "repurchase" => {
                let inner_props = serde_json::Map::try_from(json::ValueConverter(inner_value))?;
                let mins = json::transform_optional_field(
                    &inner_props,
                    "minimums",
                    Box::new(|v| Minimums::try_from(&v)),
                )?;
                let mechanics = Mechanics::try_from(&json::get_object(&inner_props, "mechanics")?)?;
                Ok(Enhance::Repurchase(mins, Box::new(mechanics)))
            }
            _ => Err(PathedError::from("unexpected field")),
        }
    }
}

#[cfg(test)]
mod test_enhancements {
    use super::*;

    #[test]
    fn no_minimums() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "repurchase": {
                        "mechanics": {
                            "free-text": "Something is different."
                        }
                    }
                }
            "#,
        )
        .unwrap();
        let built = Enhance::try_from(json.as_object().unwrap()).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|enh| enh.to_string()),
            Ok("With a repurchase: Something is different.".to_string())
        )
    }

    #[test]
    fn minimum_essence() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "repurchase": {
                        "minimums": {
                            "essence": 2
                        },
                        "mechanics": {
                            "free-text": "Something is different."
                        }
                    }
                }
            "#,
        )
        .unwrap();
        let built = Enhance::try_from(json.as_object().unwrap()).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|enh| enh.to_string()),
            Ok("With a repurchase at Essence 2: Something is different.".to_string())
        )
    }

    #[test]
    fn minimum_essence_and_ability() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "repurchase": {
                        "minimums": {
                            "essence": 2,
                            "ability": { "Martial Arts": 4 }
                        },
                        "mechanics": {
                            "free-text": "Something is different."
                        }
                    }
                }
            "#,
        )
        .unwrap();
        let built = Enhance::try_from(json.as_object().unwrap()).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|enh| enh.to_string()),
            Ok(
                "With a repurchase at Martial Arts 4, Essence 2: Something is different."
                    .to_string()
            )
        )
    }
}
