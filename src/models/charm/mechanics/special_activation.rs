use crate::models::time::TimeUnit;
use crate::util::json;
use json::PathedError;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub enum SpecialActivation {
    Rules(Vec<SpecialActivationRule>),
    Follows(String, Option<String>),
}

impl Display for SpecialActivation {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let SpecialActivation::Follows(charm, exception) = self {
            write!(
                f,
                "This Charm follows the **special activation rules** of {}",
                charm
            )?;
            if let Some(exn) = exception {
                write!(f, ", but {}", exn)?;
            }
            write!(f, ".")
        } else {
            write!(f, "**Special activation rules**: ")?;
            match self {
                SpecialActivation::Rules(rules) => write!(
                    f,
                    "{}",
                    rules
                        .iter()
                        .map(|r| r.to_string())
                        .collect::<Vec<String>>()
                        .join(" ")
                ),
                SpecialActivation::Follows(_, _) => panic!("Unreachable case"),
            }
        }
    }
}

impl TryFrom<&serde_json::Map<String, serde_json::Value>> for SpecialActivation {
    type Error = PathedError;

    fn try_from(value: &serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        let (key, inner_value) = json::get_singular(value)?;
        match key.as_str() {
            "rules" => Ok(SpecialActivation::Rules(json::map_array(
                Vec::try_from(json::ValueConverter(inner_value))?,
                Box::new(|v| SpecialActivationRule::try_from(&v)),
            )?)),
            "follows" => match inner_value {
                serde_json::Value::String(charm) => {
                    Ok(SpecialActivation::Follows(charm.to_owned(), None))
                }
                serde_json::Value::Object(m) => {
                    let charm = json::get_string(m, "charm")?;
                    let exception = json::get_string(m, "exception")?;
                    Ok(SpecialActivation::Follows(charm, Some(exception)))
                }
                _ => Err(PathedError::from("must be a String or an Object")),
            },
            _ => Err(PathedError::from("unexpected field")),
        }
    }
}

#[cfg(test)]
mod test_special_activation {
    use super::*;

    #[test]
    fn rules() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "rules": [
                        { "free-text": "Some rules" }
                    ]
                }
            "#,
        )
        .unwrap();
        let built =
            SpecialActivation::try_from(json.as_object().unwrap()).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sa| sa.to_string()),
            Ok("**Special activation rules**: Some rules".to_string())
        )
    }

    #[test]
    fn follows_charm() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "follows": "Some Charm"
                }
            "#,
        )
        .unwrap();
        let built =
            SpecialActivation::try_from(json.as_object().unwrap()).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sa| sa.to_string()),
            Ok("This Charm follows the **special activation rules** of Some Charm.".to_string())
        )
    }

    #[test]
    fn follows_charm_exception() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "follows": {
                        "charm": "Some Charm",
                        "exception": "doesn't do something"
                    }
                }
            "#,
        )
        .unwrap();
        let built =
            SpecialActivation::try_from(json.as_object().unwrap()).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sa| sa.to_string()),
            Ok("This Charm follows the **special activation rules** of Some Charm, but doesn't do something.".to_string())
        )
    }
}

pub enum SpecialActivationRule {
    FreeText(String),
    Reflexive(String),
    OncePer(SpecialActivationRulePer, Option<String>),
}

impl Display for SpecialActivationRule {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            SpecialActivationRule::FreeText(s) => write!(f, "{}", s),
            SpecialActivationRule::Reflexive(condition) => write!(
                f,
                "Whenever {}, this Charm may be activated reflexively.",
                condition
            ),
            SpecialActivationRule::OncePer(per, None) => {
                write!(f, "This Charm can only be used once {}.", per)
            }
            SpecialActivationRule::OncePer(per, Some(reset)) => write!(
                f,
                "This Charm can only be used once {}, but can be reset by {}.",
                per, reset
            ),
        }
    }
}

impl TryFrom<&serde_json::Value> for SpecialActivationRule {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        let props = serde_json::Map::try_from(json::ValueConverter(value))?;
        let (key, inner_value) = json::get_singular(&props)?;
        match key.as_str() {
            "free-text" => Ok(SpecialActivationRule::FreeText(String::try_from(
                json::ValueConverter(inner_value),
            )?)),
            "reflexive" => Ok(SpecialActivationRule::Reflexive(String::try_from(
                json::ValueConverter(inner_value),
            )?)),
            "once-per" => {
                let inner_props = serde_json::Map::try_from(json::ValueConverter(inner_value))?;
                let per = match (
                    inner_props.contains_key("duration"),
                    inner_props.contains_key("event"),
                ) {
                    (true, false) => SpecialActivationRulePer::Duration(TimeUnit::try_from(
                        &json::get_string(&inner_props, "duration")?,
                    )?),
                    (false, true) => {
                        SpecialActivationRulePer::Event(json::get_string(&inner_props, "event")?)
                    }
                    (true, true) => SpecialActivationRulePer::EventInDuration(
                        json::get_string(&inner_props, "event")?,
                        TimeUnit::try_from(&json::get_string(&inner_props, "duration")?)?,
                    ),
                    (false, false) => Err(PathedError::Field(
                        "once-per".to_string(),
                        Box::new(PathedError::from("must have duration or event")),
                    ))?,
                };
                let reset = json::transform_optional_field(
                    &inner_props,
                    "reset",
                    Box::new(|v| String::try_from(json::ValueConverter(&v))),
                )?;
                Ok(SpecialActivationRule::OncePer(per, reset))
            }
            _ => Err(PathedError::from("unexpected field")),
        }
    }
}

#[cfg(test)]
mod test_special_activation_rules {
    use super::*;

    #[test]
    fn free_text() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "free-text": "Some rules."
                }
            "#,
        )
        .unwrap();
        let built = SpecialActivationRule::try_from(&json).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sar| sar.to_string()),
            Ok("Some rules.".to_string())
        )
    }

    #[test]
    fn reflexive_activation() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "reflexive": "something happens"
                }
            "#,
        )
        .unwrap();
        let built = SpecialActivationRule::try_from(&json).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sar| sar.to_string()),
            Ok("Whenever something happens, this Charm may be activated reflexively.".to_string())
        )
    }

    #[test]
    fn once_per_duration_no_reset() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "once-per": {
                        "duration": "scene"
                    }
                }
            "#,
        )
        .unwrap();
        let built = SpecialActivationRule::try_from(&json).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sar| sar.to_string()),
            Ok("This Charm can only be used once per scene.".to_string())
        )
    }

    #[test]
    fn once_per_event_no_reset() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "once-per": {
                        "event": "visit"
                    }
                }
            "#,
        )
        .unwrap();
        let built = SpecialActivationRule::try_from(&json).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sar| sar.to_string()),
            Ok("This Charm can only be used once per visit.".to_string())
        )
    }

    #[test]
    fn once_per_duration_with_reset() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "once-per": {
                        "duration": "scene",
                        "reset": "crashing her opponent"
                    }
                }
            "#,
        )
        .unwrap();
        let built = SpecialActivationRule::try_from(&json).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sar| sar.to_string()),
            Ok("This Charm can only be used once per scene, but can be reset by crashing her opponent.".to_string())
        )
    }

    #[test]
    fn once_per_duration_and_event() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "once-per": {
                        "duration": "scene",
                        "event": "target"
                    }
                }
            "#,
        )
        .unwrap();
        let built = SpecialActivationRule::try_from(&json).map_err(|e| e.to_string());
        assert_eq!(
            built.map(|sar| sar.to_string()),
            Ok("This Charm can only be used once per target, per scene.".to_string())
        )
    }
}

pub enum SpecialActivationRulePer {
    Duration(TimeUnit),
    Event(String),
    EventInDuration(String, TimeUnit),
}

impl Display for SpecialActivationRulePer {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            SpecialActivationRulePer::Duration(time) => write!(f, "per {}", time),
            SpecialActivationRulePer::Event(event) => write!(f, "per {}", event),
            SpecialActivationRulePer::EventInDuration(event, time) => {
                write!(f, "per {}, per {}", event, time)
            }
        }
    }
}
