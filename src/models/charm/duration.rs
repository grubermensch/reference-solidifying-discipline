use crate::models::quantity::Quantity;
use crate::models::time::TimeUnit;
use crate::util::json;
use crate::util::json::PathedError;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub enum Duration {
    Instant,
    Time(TimeUnit, Quantity),
    Until(String),
    Indefinite,
    Permanent,
}

impl Display for Duration {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Duration::Instant => "Instant".to_string(),
                Duration::Time(unit, count) => {
                    let unit_word = if count == &Quantity::Number(1) {
                        unit.to_string()
                    } else {
                        unit.display_plural().to_string()
                    };
                    format!("{} {}", count, unit_word)
                }
                Duration::Until(condition) => format!("Until {}", condition),
                Duration::Indefinite => "Indefinite".to_string(),
                Duration::Permanent => "Permanent".to_string(),
            }
        )
    }
}

impl TryFrom<&serde_json::Value> for Duration {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        match value {
            serde_json::Value::String(s) => match s.to_lowercase().as_str() {
                "instant" => Ok(Duration::Instant),
                "indefinite" => Ok(Duration::Indefinite),
                "permanent" => Ok(Duration::Permanent),
                _ => Err(PathedError::from("unexpected value")),
            },
            serde_json::Value::Object(m) => {
                let (key, inner_value) = json::get_singular(m)?;
                match key.as_str() {
                    "until" => Ok(Duration::Until(json::get_string(m, "until")?)),
                    _ => {
                        let unit = TimeUnit::try_from(&key).map_err(|e| PathedError::Raw(e))?;
                        let amount = Quantity::try_from(inner_value)
                            .map_err(|e| PathedError::Field(key, Box::new(e)))?;
                        Ok(Duration::Time(unit, amount))
                    }
                }
            }
            _ => Err(PathedError::from("must be a String or an Object")),
        }
    }
}

#[cfg(test)]
mod test_charm_duration {
    use super::*;

    #[test]
    fn singular_time() {
        assert_eq!(
            Duration::Time(TimeUnit::Day, Quantity::Number(1)).to_string(),
            "One day"
        )
    }

    #[test]
    fn plural_time() {
        assert_eq!(
            Duration::Time(TimeUnit::Day, Quantity::Number(2)).to_string(),
            "Two days"
        )
    }

    #[test]
    fn plural_variant_time() {
        assert_eq!(
            Duration::Time(TimeUnit::Story, Quantity::Number(2)).to_string(),
            "Two stories"
        )
    }

    #[test]
    fn dashed_count() {
        assert_eq!(
            Duration::Time(TimeUnit::Day, Quantity::Number(79)).to_string(),
            "Seventy-nine days"
        )
    }

    #[test]
    fn multiword_count() {
        assert_eq!(
            Duration::Time(TimeUnit::Day, Quantity::Number(1337)).to_string(),
            "One thousand three hundred thirty-seven days"
        )
    }
}
