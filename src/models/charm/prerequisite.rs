use crate::util::inline_list::InlineList;
use crate::util::json;
use crate::util::json::PathedError;
use numeral::Cardinal;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub enum Prerequisite {
    Charm(String),
    StackedCharm(String, u16),
    Or(Vec<Prerequisite>),
    Any(u16, PrerequisitePattern),
    None,
}

impl Display for Prerequisite {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Prerequisite::Charm(name) => name.to_owned(),
                Prerequisite::StackedCharm(name, multiple) => format!("{} (x{})", name, multiple),
                Prerequisite::Or(prereqs) => prereqs
                    .iter()
                    .map(|pr| pr.to_string())
                    .collect::<Vec<String>>()
                    .join(" or "),
                Prerequisite::Any(count, pattern) => format!(
                    "Any {} {}",
                    count.cardinal(),
                    if count == &(1 as u16) {
                        pattern.to_string()
                    } else {
                        pattern.display_plural().to_string()
                    }
                ),
                Prerequisite::None => "None".to_string(),
            }
        )
    }
}

impl TryFrom<&serde_json::Map<String, serde_json::Value>> for Prerequisite {
    type Error = PathedError;

    fn try_from(value: &serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        let (key, _) = json::get_singular(value)?;
        match key.as_str() {
            "charm" => Ok(Prerequisite::Charm(json::get_string(value, "charm")?)),
            "stacked-charm" => json::transform_field(
                value,
                "stacked-charm",
                Box::new(|v| {
                    Vec::try_from(json::ValueConverter(&v)).and_then(|array| {
                        match array.as_slice() {
                            [serde_json::Value::String(name), serde_json::Value::Number(_)] => {
                                let count = u16::try_from(json::ValueConverter(&array[1]))?;
                                Ok(Prerequisite::StackedCharm(name.to_owned(), count))
                            }
                            _ => Err(PathedError::from(
                                "must be of the form [\"charm name\", count]",
                            )),
                        }
                    })
                }),
            ),
            "or" => json::transform_field(
                value,
                "or",
                Box::new(|v| {
                    Vec::try_from(json::ValueConverter(&v)).and_then(|array| {
                        json::map_array(array, Box::new(|p| Prerequisite::try_from(&p)))
                            .map(|ps| Prerequisite::Or(ps))
                    })
                }),
            ),
            "any" => {
                let inner_value = json::get_object(value, "any")?;
                let count = json::get_u16(&inner_value, "count")
                    .map_err(|e| PathedError::Field("any".to_string(), Box::new(e)))?;
                let without_count = {
                    let mut m = inner_value.clone();
                    m.remove("count").unwrap();
                    m
                };
                let pattern = json::get_singular(&without_count)
                    .and_then(|(k, v)| PrerequisitePattern::try_from((k, v)))
                    .map_err(|e| PathedError::Field("any".to_string(), Box::new(e)))?;
                Ok(Prerequisite::Any(count, pattern))
            }
            "none" => Ok(Prerequisite::None),
            _ => Err(PathedError::from("unexpected key")),
        }
    }
}

impl TryFrom<&serde_json::Value> for Prerequisite {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        Prerequisite::try_from(&serde_json::Map::try_from(json::ValueConverter(&value))?)
    }
}

#[cfg(test)]
mod test_charm_prerequisite {
    use super::*;

    #[test]
    fn any_one_charms() {
        assert_eq!(
            Prerequisite::Any(1, PrerequisitePattern::Abilities(vec!["Brawl".to_string()]))
                .to_string(),
            "Any one Brawl Charm"
        )
    }

    #[test]
    fn any_charms_one_ability() {
        assert_eq!(
            Prerequisite::Any(3, PrerequisitePattern::Abilities(vec!["Brawl".to_string()]))
                .to_string(),
            "Any three Brawl Charms"
        )
    }
}

pub enum PrerequisitePattern {
    Charms(Vec<String>),
    Abilities(Vec<String>),
}

impl PrerequisitePattern {
    pub fn display_plural(&self) -> impl Display + '_ {
        PPDisplayPlural(self)
    }
}

impl Display for PrerequisitePattern {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            PrerequisitePattern::Charms(cs) => write!(f, "of {}", cs.join(" or ")),
            PrerequisitePattern::Abilities(abs) => {
                write!(f, "{} Charm", abs.to_inline_list("or".to_string()))
            }
        }
    }
}

impl TryFrom<(String, &serde_json::Value)> for PrerequisitePattern {
    type Error = PathedError;

    fn try_from((kind, pattern): (String, &serde_json::Value)) -> Result<Self, Self::Error> {
        match kind.as_str() {
            "charms" => Vec::try_from(json::ValueConverter(pattern))
                .and_then(|values| {
                    json::map_array(
                        values,
                        Box::new(|c| String::try_from(json::ValueConverter(&c))),
                    )
                })
                .and_then(|charm_names| Ok(PrerequisitePattern::Charms(charm_names)))
                .map_err(|e| PathedError::Field("charms".to_string(), Box::new(e))),
            "abilities" => Vec::try_from(json::ValueConverter(pattern))
                .and_then(|values| {
                    json::map_array(
                        values,
                        Box::new(|c| String::try_from(json::ValueConverter(&c))),
                    )
                })
                .and_then(|ability_names| Ok(PrerequisitePattern::Abilities(ability_names)))
                .map_err(|e| PathedError::Field("abilities".to_string(), Box::new(e))),
            _ => Err(PathedError::from("unexpected key")),
        }
    }
}

pub struct PPDisplayPlural<'a>(&'a PrerequisitePattern);

impl Display for PPDisplayPlural<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            PrerequisitePattern::Charms(cs) => write!(f, "of {}", cs.join(" or ")),
            PrerequisitePattern::Abilities(abs) => {
                write!(f, "{} Charms", abs.to_inline_list("or".to_string()))
            }
        }
    }
}

#[cfg(test)]
mod test_prerequisite_pattern {
    use super::*;

    #[test]
    fn one_charm() {
        assert_eq!(
            PrerequisitePattern::Charms(vec!["Some Charm".to_string()]).to_string(),
            "of Some Charm"
        )
    }

    #[test]
    fn two_charms() {
        assert_eq!(
            PrerequisitePattern::Charms(vec![
                "Some Charm".to_string(),
                "Another Charm".to_string(),
            ])
            .to_string(),
            "of Some Charm or Another Charm"
        )
    }

    #[test]
    fn three_charms() {
        assert_eq!(
            PrerequisitePattern::Charms(vec![
                "Some Charm".to_string(),
                "Another Charm".to_string(),
                "Third Charm".to_string(),
            ])
            .to_string(),
            "of Some Charm or Another Charm or Third Charm"
        )
    }

    #[test]
    fn one_ability() {
        assert_eq!(
            PrerequisitePattern::Abilities(vec!["Brawl".to_string()]).to_string(),
            "Brawl Charm"
        )
    }

    #[test]
    fn two_abilities() {
        assert_eq!(
            PrerequisitePattern::Abilities(vec!["Brawl".to_string(), "Martial Arts".to_string(),])
                .to_string(),
            "Brawl or Martial Arts Charm"
        )
    }

    #[test]
    fn three_abilities() {
        assert_eq!(
            PrerequisitePattern::Abilities(vec![
                "Brawl".to_string(),
                "Martial Arts".to_string(),
                "Melee".to_string(),
            ])
            .to_string(),
            "Brawl, Martial Arts or Melee Charm"
        )
    }
}
