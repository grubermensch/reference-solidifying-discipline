use crate::util::json;
use crate::util::json::PathedError;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub struct Minimums {
    pub essence: u16,
    pub ability: Option<(String, u16)>,
}

impl Display for Minimums {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some((ability, rating)) = &self.ability {
            write!(f, "{} {}, ", ability, rating)?;
        }
        write!(f, "Essence {}", &self.essence)
    }
}

impl TryFrom<&serde_json::Map<String, serde_json::Value>> for Minimums {
    type Error = PathedError;

    fn try_from(value: &serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        let essence = json::get_u16(value, "essence")?;
        let ability = json::transform_optional_field(
            value,
            "ability",
            Box::new(|v| {
                serde_json::Map::try_from(json::ValueConverter(&v))
                    .and_then(|m| json::get_singular(&m).map(|(k, v)| (k, v.clone())))
                    .and_then(|(ability, inner_value)| {
                        match u16::try_from(json::ValueConverter(&inner_value)) {
                            Ok(rating) => Ok((ability, rating)),
                            Err(e) => Err(PathedError::Field(ability, Box::new(e))),
                        }
                    })
            }),
        )?;

        Ok(Minimums { essence, ability })
    }
}

impl TryFrom<&serde_json::Value> for Minimums {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        Minimums::try_from(&serde_json::Map::try_from(json::ValueConverter(&value))?)
    }
}

#[cfg(test)]
mod test_charm_minimums {
    use super::*;

    #[test]
    fn essence() {
        let charm_minimums = Minimums {
            essence: 7,
            ability: None,
        };

        assert_eq!(charm_minimums.to_string(), "Essence 7")
    }

    #[test]
    fn ability_essence() {
        let charm_minimums = Minimums {
            essence: 3,
            ability: Some(("Fooasdfbar".to_string(), 5)),
        };

        assert_eq!(charm_minimums.to_string(), "Fooasdfbar 5, Essence 3")
    }

    #[test]
    fn error_no_essence() {
        let parsed: serde_json::Value = serde_json::from_str("{}").unwrap();

        assert_eq!(
            Minimums::try_from(&parsed).err().unwrap().to_string(),
            ".essence: missing field"
        )
    }

    #[test]
    fn ok_only_essence() {
        let parsed: serde_json::Value = serde_json::from_str("{ \"essence\": 2 }").unwrap();

        assert!(Minimums::try_from(&parsed).is_ok())
    }

    #[test]
    fn error_ability_rating_too_big() {
        let parsed: serde_json::Value =
            serde_json::from_str("{ \"essence\": 2, \"ability\": { \"Sail\": 1234567890 } }")
                .unwrap();

        assert_eq!(
            Minimums::try_from(&parsed).err().unwrap().to_string(),
            ".ability.Sail: maximum value 65535"
        )
    }
}
