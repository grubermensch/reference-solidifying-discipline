use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub enum Typ {
    Simple,
    Supplemental,
    Reflexive,
    Permanent,
}

impl Display for Typ {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Typ::Simple => "Simple",
                Typ::Supplemental => "Supplemental",
                Typ::Reflexive => "Reflexive",
                Typ::Permanent => "Permanent",
            }
        )
    }
}

impl TryFrom<String> for Typ {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.to_lowercase().as_str() {
            "simple" => Ok(Typ::Simple),
            "supplemental" => Ok(Typ::Supplemental),
            "reflexive" => Ok(Typ::Reflexive),
            "permanent" => Ok(Typ::Permanent),
            _ => Err(format!("Unexpected Charm type: {}", value)),
        }
    }
}
