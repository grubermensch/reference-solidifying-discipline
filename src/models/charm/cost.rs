#[cfg(test)]
use maplit::btreemap;

use crate::models::resource::ResourceKind;
use crate::util::json;
use crate::util::json::PathedError;
use std::collections::BTreeMap;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub enum Cost {
    Raw(RawCost),
    Per {
        base: Option<Box<Cost>>,
        incremental: Box<Cost>,
        condition: String,
    },
    Optional {
        base: Box<Cost>,
        maybe: Box<Cost>,
    },
    Or(Vec<Cost>),
}

#[cfg(test)]
impl Cost {
    pub fn none() -> Self {
        Cost::Raw(RawCost {
            resources: btreemap! {},
            other: None,
        })
    }
}

impl Display for Cost {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Cost::Raw(raw) => write!(f, "{}", raw),
            Cost::Per {
                base: None,
                incremental,
                condition,
            } => write!(f, "{} per {}", incremental, condition),
            Cost::Per {
                base: Some(base),
                incremental,
                condition,
            } => write!(f, "{}, plus {} per {}", base, incremental, condition),
            Cost::Optional { base, maybe } => write!(f, "{}({})", base, maybe),
            Cost::Or(costs) => write!(
                f,
                "{}",
                costs
                    .iter()
                    .map(|c| c.to_string())
                    .collect::<Vec<String>>()
                    .join(" or ")
            ),
        }
    }
}

impl TryFrom<&serde_json::Map<String, serde_json::Value>> for Cost {
    type Error = PathedError;

    fn try_from(value: &serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        if value.len() == 1 {
            match value.keys().next().unwrap().as_str() {
                "per" => {
                    let props = json::get_object(value, "per")?;
                    match json::transform_field(
                        &props,
                        "incremental",
                        Box::new(|v| Cost::try_from(&v).map(Box::new)),
                    )
                    .and_then(|incremental| {
                        json::transform_optional_field(
                            &props,
                            "base",
                            Box::new(|v| Cost::try_from(&v).map(Box::new)),
                        )
                        .map(|base| (incremental, base))
                    })
                    .and_then(|(incremental, base)| {
                        json::get_string(&props, "condition")
                            .map(|condition| (incremental, base, condition))
                    }) {
                        Ok((incremental, base, condition)) => Ok(Cost::Per {
                            base,
                            incremental,
                            condition,
                        }),
                        Err(e) => Err(PathedError::Field("per".to_string(), Box::new(e))),
                    }
                }
                "optional" => {
                    let props = json::get_object(value, "optional")?;
                    match json::transform_field(
                        &props,
                        "base",
                        Box::new(|v| Cost::try_from(&v).map(Box::new)),
                    )
                    .and_then(|base| {
                        json::transform_field(
                            &props,
                            "maybe",
                            Box::new(|v| Cost::try_from(&v).map(Box::new)),
                        )
                        .map(|maybe| (base, maybe))
                    }) {
                        Ok((base, maybe)) => Ok(Cost::Optional { base, maybe }),
                        Err(e) => Err(PathedError::Field("optional".to_string(), Box::new(e))),
                    }
                }
                "or" => match json::map_array(
                    json::get_array(value, "or")?,
                    Box::new(|c| Cost::try_from(&c)),
                ) {
                    Ok(costs) => Ok(Cost::Or(costs)),
                    Err(e) => Err(PathedError::Field("or".to_string(), Box::new(e))),
                },
                _ => RawCost::try_from(value).map(Cost::Raw),
            }
        } else {
            RawCost::try_from(value).map(Cost::Raw)
        }
    }
}

impl TryFrom<&serde_json::Value> for Cost {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        Cost::try_from(&serde_json::Map::try_from(json::ValueConverter(&value))?)
    }
}

pub struct RawCost {
    pub resources: BTreeMap<ResourceKind, u16>,
    pub other: Option<Vec<String>>,
}

impl Display for RawCost {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut costs = vec![];

        for (kind, num) in &self.resources {
            costs.push(format!("{}{}", num, kind.display_short()));
        }
        if let Some(others) = &self.other {
            costs.append(&mut others.clone());
        }

        if costs.is_empty() {
            write!(f, "\u{2014}")
        } else {
            write!(f, "{}", costs.join(", "))
        }
    }
}

impl TryFrom<&serde_json::Map<String, serde_json::Value>> for RawCost {
    type Error = PathedError;

    fn try_from(value: &serde_json::Map<String, serde_json::Value>) -> Result<Self, Self::Error> {
        let mut resources = BTreeMap::new();
        let mut other_costs = vec![];
        for key in value.keys() {
            if key.to_lowercase().as_str() == "other" {
                other_costs.append(&mut json::map_array(
                    json::get_array(value, "other")?,
                    Box::new(|v| String::try_from(json::ValueConverter(&v))),
                )?)
            } else {
                let kind = ResourceKind::try_from(key).map_err(|e| {
                    PathedError::Field(key.to_string(), Box::new(PathedError::Raw(e)))
                })?;
                let amount = json::get_u16(value, key)?;

                resources.insert(kind, amount);
            }
        }

        Ok(RawCost {
            resources,
            other: if other_costs.is_empty() {
                None
            } else {
                Some(other_costs)
            },
        })
    }
}

#[cfg(test)]
mod test_cost {
    use super::*;

    #[test]
    fn dash_if_all_none() {
        let charm_cost = Cost::Raw(RawCost {
            resources: btreemap! {},
            other: None,
        });

        assert_eq!(charm_cost.to_string(), "\u{2014}")
    }

    #[test]
    fn motes_before_willpower() {
        let charm_cost = Cost::Raw(RawCost {
            resources: btreemap! {
                ResourceKind::Mote => 2,
                ResourceKind::Willpower => 3,
            },
            other: None,
        });

        assert_eq!(charm_cost.to_string(), "2m, 3wp")
    }

    #[test]
    fn health_levels_ordered() {
        let charm_cost = Cost::Raw(RawCost {
            resources: btreemap! {
                ResourceKind::BashingHealthLevel => 7,
                ResourceKind::LethalHealthLevel => 8,
                ResourceKind::AggravatedHealthLevel => 9,
            },
            other: None,
        });

        assert_eq!(charm_cost.to_string(), "7hl, 8lhl, 9ahl")
    }

    #[test]
    fn craft_experience_ordered() {
        let charm_cost = Cost::Raw(RawCost {
            resources: btreemap! {
                ResourceKind::SilverExperience => 9,
                ResourceKind::GoldExperience => 16,
                ResourceKind::WhiteExperience => 25,
            },
            other: None,
        });

        assert_eq!(charm_cost.to_string(), "9sxp, 16gxp, 25wxp")
    }
}
