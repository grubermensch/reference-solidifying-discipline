use crate::util::json;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

mod cost;
use cost::Cost;
mod minimums;
use minimums::Minimums;
mod typ;
use typ::Typ;
mod duration;
use duration::Duration;
mod prerequisite;
use crate::util::json::PathedError;
use prerequisite::Prerequisite;
mod mechanics;
use mechanics::Mechanics;

pub struct Charm {
    pub name: String,
    pub cost: Cost,
    pub minimums: Minimums,
    pub ty: Typ,
    pub keywords: Vec<String>,
    pub duration: Duration,
    pub prerequisites: Vec<Prerequisite>,
    pub mechanics: Mechanics,
}

impl Charm {
    fn fmt_fields(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let field_lines = vec![
            format!(
                "**Cost**: {}; **Mins**: {}",
                self.cost.to_string(),
                self.minimums.to_string()
            ),
            format!("**Type**: {}", self.ty.to_string()),
            format!(
                "**Keywords**: {}",
                if self.keywords.is_empty() {
                    "None".to_string()
                } else {
                    self.keywords.join(", ")
                }
            ),
            format!("**Duration**: {}", self.duration.to_string()),
            format!(
                "**Prerequisite Charms**: {}",
                if self.prerequisites.is_empty() {
                    "None".to_string()
                } else {
                    self.prerequisites
                        .iter()
                        .map(|pr| pr.to_string())
                        .collect::<Vec<String>>()
                        .join(", ")
                }
            ),
        ];

        write!(f, "{}", field_lines.join("<br/>\n"))
    }
}

impl Display for Charm {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "# {}\n\n", self.name)?;
        self.fmt_fields(f)?;
        write!(f, "\n\n{}", self.mechanics)
    }
}

impl TryFrom<&serde_json::Value> for Charm {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        if let serde_json::Value::Object(charm_props) = value {
            Ok(Charm {
                name: json::get_string(charm_props, "name")?,
                cost: json::transform_field(charm_props, "cost", Box::new(|v| Cost::try_from(&v)))?,
                minimums: json::transform_field(
                    charm_props,
                    "minimums",
                    Box::new(|v| Minimums::try_from(&v)),
                )?,
                ty: Typ::try_from(json::get_string(charm_props, "type")?).map_err(|e| {
                    PathedError::Field("type".to_string(), Box::new(PathedError::Raw(e)))
                })?,
                keywords: json::transform_field(
                    charm_props,
                    "keywords",
                    Box::new(|v| {
                        json::map_array(
                            Vec::try_from(json::ValueConverter(&v))?,
                            Box::new(|keyword| String::try_from(json::ValueConverter(&keyword))),
                        )
                    }),
                )?,
                duration: json::transform_field(
                    charm_props,
                    "duration",
                    Box::new(|v| Duration::try_from(&v)),
                )?,
                prerequisites: json::transform_field(
                    charm_props,
                    "prerequisites",
                    Box::new(|v| {
                        Vec::try_from(json::ValueConverter(&v)).and_then(|array| {
                            json::map_array(array, Box::new(|p| Prerequisite::try_from(&p)))
                        })
                    }),
                )?,
                mechanics: Mechanics::try_from(&json::get_object(charm_props, "mechanics")?)
                    .map_err(|e| PathedError::Field("mechanics".to_string(), Box::new(e)))?,
            })
        } else {
            Err(PathedError::from("Charm must be an Object"))
        }
    }
}

#[cfg(test)]
mod test_charm {
    use super::*;
    use regex::Regex;

    #[test]
    fn starts_with_name() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "name": "Trivial Charm Name",
                    "cost": {},
                    "minimums": {
                        "essence": 1
                    },
                    "type": "simple",
                    "keywords": [],
                    "duration": "instant",
                    "prerequisites": [],
                    "mechanics": {
                        "free-text": "Trivial description."
                    }
                }
            "#,
        )
        .unwrap();
        let built = Charm::try_from(&json).map_err(|e| e.to_string());
        assert!(built
            .unwrap()
            .to_string()
            .lines()
            .nth(0)
            .unwrap()
            .contains("Trivial Charm Name"))
    }

    #[test]
    fn fields_in_correct_order() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "name": "Trivial Charm Name",
                    "cost": {},
                    "minimums": {
                        "essence": 1
                    },
                    "type": "simple",
                    "keywords": [],
                    "duration": "instant",
                    "prerequisites": [],
                    "mechanics": {
                        "free-text": "Trivial description."
                    }
                }
            "#,
        )
        .unwrap();
        let built = Charm::try_from(&json).map_err(|e| e.to_string());
        let field_label_matcher = Regex::new(r"\*\*[^\*]+\*\*:").unwrap();
        let expected_labels: Vec<String> = vec![
            "Cost",
            "Mins",
            "Type",
            "Keywords",
            "Duration",
            "Prerequisite Charms",
        ]
        .iter()
        .map(|l| format!("**{}**:", l))
        .collect();
        assert_eq!(
            field_label_matcher
                .find_iter(built.unwrap().to_string().as_str())
                .map(|m| m.as_str())
                .collect::<Vec<&str>>(),
            expected_labels
        )
    }

    #[test]
    fn none_if_empty_keywords() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "name": "Trivial Charm Name",
                    "cost": {},
                    "minimums": {
                        "essence": 1
                    },
                    "type": "simple",
                    "keywords": [],
                    "duration": "instant",
                    "prerequisites": [],
                    "mechanics": {
                        "free-text": "Trivial description."
                    }
                }
            "#,
        )
        .unwrap();
        let built = Charm::try_from(&json).map_err(|e| e.to_string());

        assert!(built.unwrap().to_string().contains("**Keywords**: None"))
    }

    #[test]
    fn none_if_empty_prereqs() {
        let json: serde_json::Value = serde_json::from_str(
            r#"
                {
                    "name": "Trivial Charm Name",
                    "cost": {},
                    "minimums": {
                        "essence": 1
                    },
                    "type": "simple",
                    "keywords": [],
                    "duration": "instant",
                    "prerequisites": [],
                    "mechanics": {
                        "free-text": "Trivial description."
                    }
                }
            "#,
        )
        .unwrap();
        let built = Charm::try_from(&json).map_err(|e| e.to_string());
        assert!(built
            .unwrap()
            .to_string()
            .contains("**Prerequisite Charms**: None"))
    }
}
