use crate::util::json;
use crate::util::json::PathedError;
use crate::util::sentence_case::SentenceCase;
use numeral::Cardinal;
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

#[derive(PartialEq, Eq)]
pub enum Quantity {
    Number(u16),
    Sum(Vec<Quantity>),
    Term(String),
}

impl Quantity {
    fn display_internal(&self) -> impl Display + '_ {
        DisplayInternal(self)
    }
}

impl Display for Quantity {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Quantity::Number(num) => write!(f, "{}", num.cardinal().to_sentence_case()),
            Quantity::Sum(qs) => write!(
                f,
                "({})",
                qs.iter()
                    .map(|q| q.display_internal().to_string())
                    .collect::<Vec<String>>()
                    .join(" + ")
            ),
            Quantity::Term(t) => write!(f, "({})", t),
        }
    }
}

impl TryFrom<&serde_json::Value> for Quantity {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        match value {
            serde_json::Value::Number(_) => {
                u16::try_from(json::ValueConverter(&value)).map(Quantity::Number)
            }
            serde_json::Value::String(s) => Ok(Quantity::Term(s.to_owned())),
            serde_json::Value::Object(props) => {
                let (key, _) = json::get_singular(props)?;
                match key.as_str() {
                    "sum" => Ok(Quantity::Sum(json::map_array(
                        json::get_array(props, "sum")?,
                        Box::new(|q| Quantity::try_from(&q)),
                    )?)),
                    _ => Err(PathedError::Field(
                        key.to_string(),
                        Box::new(PathedError::from("invalid key")),
                    )),
                }
            }
            _ => Err(PathedError::from("invalid value")),
        }
    }
}

pub struct DisplayInternal<'a>(&'a Quantity);

impl Display for DisplayInternal<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            Quantity::Number(num) => write!(f, "{}", num),
            Quantity::Sum(qs) => write!(
                f,
                "{}",
                qs.iter()
                    .map(|q| q.display_internal().to_string())
                    .collect::<Vec<String>>()
                    .join(" + ")
            ),
            Quantity::Term(t) => write!(f, "{}", t),
        }
    }
}
