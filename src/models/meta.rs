use crate::util::json;
use crate::util::json::PathedError;
use std::convert::TryFrom;

pub enum Kind {
    Charm,
    Charmset,
}

impl TryFrom<&String> for Kind {
    type Error = PathedError;

    fn try_from(value: &String) -> Result<Self, Self::Error> {
        match value.to_lowercase().as_str() {
            "charm" => Ok(Kind::Charm),
            "charmset" => Ok(Kind::Charmset),
            _ => Err(PathedError::Raw(format!(
                "{} is not a recognized meta kind",
                value
            ))),
        }
    }
}

impl TryFrom<&serde_json::Value> for Kind {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        if let serde_json::Value::Object(props) = value {
            let meta = json::get_object(props, "meta")?;
            Kind::try_from(
                &json::get_string(&meta, "kind")
                    .map_err(|e| PathedError::Field("meta".to_string(), Box::new(e)))?,
            )
            .map_err(|e| {
                PathedError::Field(
                    "meta".to_string(),
                    Box::new(PathedError::Field("kind".to_string(), Box::new(e))),
                )
            })
        } else {
            Err(PathedError::from("must be an Object"))
        }
    }
}
