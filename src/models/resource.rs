use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

#[derive(PartialOrd, Ord, PartialEq, Eq)]
pub enum ResourceKind {
    Mote,
    Initiative,
    Anima,
    Willpower,
    BashingHealthLevel,
    LethalHealthLevel,
    AggravatedHealthLevel,
    Experience,
    SilverExperience,
    GoldExperience,
    WhiteExperience,
}

impl ResourceKind {
    pub fn display_short(&self) -> impl Display + '_ {
        DisplayShort(self)
    }
}

impl Display for ResourceKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ResourceKind::Mote => "mote",
                ResourceKind::Willpower => "point of temporary willpower",
                ResourceKind::BashingHealthLevel => "bashing health level",
                ResourceKind::LethalHealthLevel => "lethal health level",
                ResourceKind::AggravatedHealthLevel => "aggravated health level",
                ResourceKind::Anima => "level of anima",
                ResourceKind::Initiative => "point of initiative",
                ResourceKind::Experience => "experience point",
                ResourceKind::SilverExperience => "silver experience point",
                ResourceKind::GoldExperience => "gold experience point",
                ResourceKind::WhiteExperience => "white experience point",
            }
        )
    }
}

impl TryFrom<&String> for ResourceKind {
    type Error = String;

    fn try_from(value: &String) -> Result<Self, Self::Error> {
        match value.to_lowercase().as_str() {
            "motes" | "m" => Ok(ResourceKind::Mote),
            "willpower" | "wp" => Ok(ResourceKind::Willpower),
            "bashing" | "hl" => Ok(ResourceKind::BashingHealthLevel),
            "lethal" | "lhl" => Ok(ResourceKind::LethalHealthLevel),
            "aggravated" | "ahl" => Ok(ResourceKind::AggravatedHealthLevel),
            "anima" | "a" => Ok(ResourceKind::Anima),
            "initiative" | "i" => Ok(ResourceKind::Initiative),
            "experience" | "xp" => Ok(ResourceKind::Experience),
            "silver-experience" | "sxp" => Ok(ResourceKind::SilverExperience),
            "gold-experience" | "gxp" => Ok(ResourceKind::GoldExperience),
            "white-experience" | "wxp" => Ok(ResourceKind::WhiteExperience),
            _ => Err("Cost object keys must be a valid resource kind or 'other'".to_string()),
        }
    }
}

pub struct DisplayShort<'a>(&'a ResourceKind);

impl Display for DisplayShort<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self.0 {
                ResourceKind::Mote => "m",
                ResourceKind::Willpower => "wp",
                ResourceKind::BashingHealthLevel => "hl",
                ResourceKind::LethalHealthLevel => "lhl",
                ResourceKind::AggravatedHealthLevel => "ahl",
                ResourceKind::Anima => "a",
                ResourceKind::Initiative => "i",
                ResourceKind::Experience => "xp",
                ResourceKind::SilverExperience => "sxp",
                ResourceKind::GoldExperience => "gxp",
                ResourceKind::WhiteExperience => "wxp",
            }
        )
    }
}
