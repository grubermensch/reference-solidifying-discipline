use crate::models::charm::Charm;
use crate::util::json::PathedError;
use crate::util::{json, markdown};
use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

pub struct Charmset {
    pub name: String,
    pub description: Option<String>,
    pub charms: Vec<Charm>,
}

impl Display for Charmset {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "# {}\n\n", &self.name)?;
        if let Some(desc) = &self.description {
            write!(f, "{}\n\n", desc)?;
        }
        for charm in &self.charms {
            write!(f, "{}\n\n", markdown::demote_headings(charm.to_string()))?;
        }
        Ok(())
    }
}

impl TryFrom<&serde_json::Value> for Charmset {
    type Error = PathedError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        if let serde_json::Value::Object(charmset_props) = value {
            Ok(Charmset {
                name: json::get_string(charmset_props, "name")?,
                description: if charmset_props.contains_key("description") {
                    Some(json::get_string(charmset_props, "description")?)
                } else {
                    None
                },
                charms: json::map_array(
                    json::get_array(charmset_props, "charms")?,
                    Box::new(|charm| Charm::try_from(&charm)),
                )
                .map_err(|e| PathedError::Field("charms".to_string(), Box::new(e)))?,
            })
        } else {
            Err(PathedError::from("Charmset must be an Object"))
        }
    }
}
