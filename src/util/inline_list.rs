pub trait InlineList {
    fn to_inline_list(&self, join_word: String) -> String;
}

impl InlineList for Vec<String> {
    fn to_inline_list(&self, join_word: String) -> String {
        match self.split_last() {
            None => "".to_string(),
            Some((item, [])) => item.to_string(),
            Some((butt, trunk)) => {
                let mut output = trunk.join(", ");
                output.push_str(format!(" {} {}", join_word, butt).as_str());
                output
            }
        }
    }
}
