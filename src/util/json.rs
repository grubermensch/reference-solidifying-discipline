use std::convert::TryFrom;
use std::error::Error;
use std::fmt::Display;

fn get_raw<'a>(
    object: &'a serde_json::Map<String, serde_json::Value>,
    key: &str,
) -> Result<&'a serde_json::Value, PathedError> {
    object.get(key).ok_or(PathedError::from("missing field"))
}

pub fn get_value<'a>(
    object: &'a serde_json::Map<String, serde_json::Value>,
    key: &str,
) -> Result<&'a serde_json::Value, String> {
    get_raw(object, key)
        .map_err(|e| PathedError::Field(key.to_string(), Box::new(e)))
        .map_err(|e| e.to_string())
}

pub fn get_string(
    object: &serde_json::Map<String, serde_json::Value>,
    key: &str,
) -> Result<String, PathedError> {
    get_raw(object, key)
        .and_then(|s| String::try_from(ValueConverter(s)))
        .map_err(|e| PathedError::Field(key.to_string(), Box::new(e)))
}

pub fn get_array(
    object: &serde_json::Map<String, serde_json::Value>,
    key: &str,
) -> Result<Vec<serde_json::Value>, PathedError> {
    get_raw(object, key)
        .and_then(|a| Vec::try_from(ValueConverter(a)))
        .map_err(|e| PathedError::Field(key.to_string(), Box::new(e)))
}

pub fn map_array<T>(
    array: Vec<serde_json::Value>,
    transform: Box<dyn Fn(serde_json::Value) -> Result<T, PathedError>>,
) -> Result<Vec<T>, PathedError> {
    array
        .into_iter()
        .enumerate()
        .try_fold(vec![], |mut acc, (idx, next)| {
            let hint = if let Some(next_props) = &next.as_object() {
                get_string(next_props, "name").ok()
            } else {
                None
            };
            match transform(next) {
                Ok(output) => {
                    acc.push(output);
                    Ok(acc)
                }
                Err(e) => Err(PathedError::Index(idx as u16, Box::new(e), hint)),
            }
        })
}

pub fn get_object(
    object: &serde_json::Map<String, serde_json::Value>,
    key: &str,
) -> Result<serde_json::Map<String, serde_json::Value>, PathedError> {
    get_raw(object, key)
        .and_then(|a| serde_json::Map::try_from(ValueConverter(a)))
        .map_err(|e| PathedError::Field(key.to_string(), Box::new(e)))
}

pub fn get_u16(
    object: &serde_json::Map<String, serde_json::Value>,
    key: &str,
) -> Result<u16, PathedError> {
    get_raw(object, key)
        .and_then(|s| u16::try_from(ValueConverter(s)))
        .map_err(|e| PathedError::Field(key.to_string(), Box::new(e)))
}

pub fn get_singular(
    object: &serde_json::Map<String, serde_json::Value>,
) -> Result<(String, &serde_json::Value), PathedError> {
    let keys = object.keys().cloned().collect::<Vec<String>>();
    if keys.len() == 1 {
        let key: String = keys.first().unwrap().to_owned();
        let value = get_value(object, &key.as_str()).unwrap();
        Ok((key, value))
    } else {
        Err(PathedError::from("must have exactly one key"))
    }
}

pub fn transform_field<T>(
    object: &serde_json::Map<String, serde_json::Value>,
    key: &str,
    transform: Box<dyn Fn(serde_json::Value) -> Result<T, PathedError>>,
) -> Result<T, PathedError> {
    get_raw(object, key)
        .and_then(|v| transform(v.to_owned()))
        .map_err(|e| PathedError::Field(key.to_string(), Box::new(e)))
}

/// Calls transform_field if the field is present, otherwise returns Ok(None)
pub fn transform_optional_field<T>(
    object: &serde_json::Map<String, serde_json::Value>,
    key: &str,
    transform: Box<dyn Fn(serde_json::Value) -> Result<T, PathedError>>,
) -> Result<Option<T>, PathedError> {
    if object.contains_key(key) {
        transform_field(object, key, transform).map(|x| Some(x))
    } else {
        Ok(None)
    }
}

/// We use the wrapper struct ValueConverter to allow conversion between Value and String, both of
/// which are external to this crate. The goal here is to cleanly return a PathedError for the rest
/// of our error handling logic to work with, and to save some space in our business logic where we
/// repeat the type conversions over and over again.
pub struct ValueConverter<'a>(pub &'a serde_json::Value);

impl TryFrom<ValueConverter<'_>> for String {
    type Error = PathedError;

    fn try_from(value: ValueConverter) -> Result<Self, Self::Error> {
        value
            .0
            .as_str()
            .ok_or(PathedError::from("must be a String"))
            .map(String::from)
    }
}

impl TryFrom<ValueConverter<'_>> for u16 {
    type Error = PathedError;

    fn try_from(value: ValueConverter) -> Result<Self, Self::Error> {
        value
            .0
            .as_u64()
            .ok_or(PathedError::from("must be an unsigned Integer"))
            .and_then(|num| {
                u16::try_from(num)
                    .map_err(|_| PathedError::Raw(format!("maximum value {}", u16::MAX)))
            })
    }
}

impl TryFrom<ValueConverter<'_>> for Vec<serde_json::Value> {
    type Error = PathedError;

    fn try_from(value: ValueConverter) -> Result<Self, Self::Error> {
        value
            .0
            .as_array()
            .ok_or(PathedError::from("must be an Array"))
            .map(|a| a.to_owned())
    }
}

impl TryFrom<ValueConverter<'_>> for serde_json::Map<String, serde_json::Value> {
    type Error = PathedError;

    fn try_from(value: ValueConverter) -> Result<Self, Self::Error> {
        value
            .0
            .as_object()
            .ok_or(PathedError::from("must be an Object"))
            .map(|a| a.to_owned())
    }
}

/// An error type for tracing errors in json conversions
///
/// It is useful to be able to describe the path through the JSON values to get to a location where
/// the actual error is occuring. This is pretty difficult to do ad-hoc. This error type allows us
/// to nest the location information and display it at the eventual handler site.
#[derive(Debug)]
pub enum PathedError {
    Index(u16, Box<PathedError>, Option<String>),
    Field(String, Box<PathedError>),
    Raw(String),
}

impl From<&str> for PathedError {
    fn from(value: &str) -> Self {
        PathedError::Raw(value.to_string())
    }
}

impl From<String> for PathedError {
    fn from(value: String) -> Self {
        PathedError::Raw(value)
    }
}

impl Display for PathedError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PathedError::Index(index, inner, None) => write!(f, "[{}]{}", index, inner),
            PathedError::Index(index, inner, Some(hint)) => {
                write!(f, "[{} ({})]{}", index, hint, inner)
            }
            PathedError::Field(label, inner) => write!(f, ".{}{}", label, inner),
            PathedError::Raw(message) => write!(f, ": {}", message),
        }
    }
}

impl Error for PathedError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            PathedError::Index(_, inner, _) => Some(inner),
            PathedError::Field(_, inner) => Some(inner),
            PathedError::Raw(_) => None,
        }
    }
}

#[cfg(test)]
mod test_pathed_error {
    use super::*;

    #[test]
    fn raw() {
        assert_eq!(PathedError::from("errmsg").to_string(), ": errmsg")
    }

    #[test]
    fn index() {
        assert_eq!(
            PathedError::Index(12, Box::new(PathedError::from("errmsg")), None).to_string(),
            "[12]: errmsg"
        )
    }

    #[test]
    fn index_with_hint() {
        assert_eq!(
            PathedError::Index(
                12,
                Box::new(PathedError::from("errmsg")),
                Some("Hint".to_string())
            )
            .to_string(),
            "[12 (Hint)]: errmsg"
        )
    }

    #[test]
    fn field() {
        assert_eq!(
            PathedError::Field("foo".to_string(), Box::new(PathedError::from("errmsg")))
                .to_string(),
            ".foo: errmsg"
        )
    }

    #[test]
    fn nested() {
        assert_eq!(
            PathedError::Index(
                5,
                Box::new(PathedError::Field(
                    "foo".to_string(),
                    Box::new(PathedError::from("errmsg")),
                )),
                None
            )
            .to_string(),
            "[5].foo: errmsg"
        )
    }
}
