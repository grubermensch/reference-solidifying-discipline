use regex::Regex;

pub fn demote_headings(source: String) -> String {
    let re = Regex::new(r"^#").unwrap();
    re.replace_all(source.as_str(), "##").to_string()
}
