use std::ops::Deref;

pub trait SentenceCase: Deref<Target = str> {
    fn to_sentence_case(&self) -> String {
        let s: &str = self.deref();
        match s.len() {
            0 => "".to_string(),
            1 => s.to_uppercase(),
            _ => {
                let (head, tail) = s.split_at(1);
                format!("{}{}", head.to_uppercase(), tail.to_lowercase())
            }
        }
    }
}

impl SentenceCase for String {}
