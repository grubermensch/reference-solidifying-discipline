# Charcoal March of Spiders Style

## Unnatural Many-Step Stride

**Cost**: 3m, plus 1m per consecutive action; **Mins**: Martial Arts 2, Essence 2<br/>
**Type**: Supplemental<br/>
**Keywords**: None<br/>
**Duration**: One action<br/>
**Prerequisite Charms**: None

The martial artist scuttles along the unseen strands of Fate woven throughout Creation, traveling wherever he is needed. This Charm supplements a movement action, allowing the character to move without regard to obstacles or even the need to rest upon solid ground. It does not increase the martial artist’s movement speed, nor does it remove the need to end movement in sufficient open space.

While moving along the strands of Fate, the martial artist climbs using his hands and feet with elbows and knees turned outward like a spider’s long legs. Non-practitioners who witness these strange movements or see the martial artist compress himself to fit through impossible spaces are often quite unsettled by the display.

The martial artist may continue to move unnaturally by reactivating this Charm for one mote on each consecutive action.

## Jumping Spider Strike

**Cost**: 1wp; **Mins**: Martial Arts 3, Essence 2<br/>
**Type**: Simple<br/>
**Keywords**: Decisive-only<br/>
**Duration**: Instant<br/>
**Prerequisite Charms**: Unnatural Many-Step Stride

This Charm allows the martial artist to make a special gambit to attack any single target that she can see. The difficulty of this gambit is equal to (the number of range bands to her target x 2). If the martial artist succeeds on the gambit, she immediately closes the distance to the target and makes a **decisive** martial arts attack using her remaining Initiative. This gambit is considered a Move action for the purposes of any charms that rely on them, including Unnatural Many-Step Stride.

## Dance of the Hungry Spider

**Cost**: —; **Mins**: Martial Arts 4, Essence 3<br/>
**Type**: Permanent<br/>
**Keywords**: Perilous<br/>
**Duration**: Permanent<br/>
**Prerequisite Charms**: Jumping Spider Strike

The martial artist moves in a sliding, shifting kata, ever trying to lure his opponents to a place of better advantage. When the martial artist is followed by an opponent after making a Disengage action, he can choose to forgo the automatic retreat movement and instead make a reflexive attack against the pursuing character.

## Maw of Dripping Venom

**Cost**: 2m; **Mins**: Martial Arts 2, Essence 2<br/>
**Type**: Supplemental<br/>
**Keywords**: Withering-only<br/>
**Duration**: Instant<br/>
**Prerequisite Charms**: None

The martial artist’s fingers needle into the target’s flesh, tainting their Essence from the inside out. This Charm supplements a **withering** attack, preventing any damage from the attack from being applied to the target’s Initiative. Instead, the attack causes the target to lose a number of motes of essence from their personal essence pool equal to the damage rolled. The martial artist does not gain any Initiative from the damage of this attack.

Using this Charm against a character that does not have a personal essence pool has no effect beyond negating the **withering** damage from the attack.

## Nest of Living Strands

**Cost**: 6m; **Mins**: Martial Arts 3, Essence 2<br/>
**Type**: Simple<br/>
**Keywords**: None<br/>
**Duration**: One scene<br/>
**Prerequisite Charms**: Maw of Dripping Venom

Hundreds of fine, transparent filaments of Essence launch from the character’s hands, weaving around the surroundings and each other to encompass everything within Short range. Any Charm that is activated within this area by spending from the character’s peripheral essence pool costs an additional mote, and those motes are absorbed immediately by the martial artist through her web. The martial artist also benefits from Essence Sight over the area of her web.

If the martial artist activates this Charm within an uncapped demesne or similar source of raw essence, she respires an additional 2 motes per round during combat.

## Water Spider Bite

**Cost**: 6m, 1wp; **Mins**: Martial Arts 5, Essence 3<br/>
**Type**: Supplemental<br/>
**Keywords**: Decisive-only<br/>
**Duration**: Instant<br/>
**Prerequisite Charms**: Nest of Living Strands

One swift blow supplemented with this Charm can render an enemy helpless. After rolling damage for a **decisive** attack supplemented with this Charm, instead of applying that damage to the target’s health track, Water Spider Bite paralyzes the target’s Essence so that they cannot recover Essence or Willpower by any means for a number of rounds equal to the damage rolled. It also physically paralyzes them for the same number of rounds, requiring that the target spends one temporary Willpower each round they wish to perform any substantial physical actions.

## Charcoal March of Spiders Form

**Cost**: 10m; **Mins**: Martial Arts 4, Essence 3<br/>
**Type**: Simple<br/>
**Keywords**: Form<br/>
**Duration**: One scene<br/>
**Prerequisite Charms**: Jumping Spider Strike, Nest of Living Strands

The martial artist moves her hands with the deft, arachnid precision of a weaver, her fingers flicking like the limbs of a spider. While Charcoal March of Spiders Form is active, she may reflexively activate Unnatural Many-Step Stride and Maw of Dripping Venom in conjunction with appropriate actions for no additional cost.

Additionally, the martial artist adds (Essence) dice to all movement and dodge actions while this Charm is active.

**Special activation rules**: Whenever the martial artist successfully makes an unexpected attack with 5+ successes, this Charm may be activated reflexively.

## Cannibalistic Heritage Technique

**Cost**: 5m, 1wp; **Mins**: Martial Arts 5, Essence 4<br/>
**Type**: Supplemental<br/>
**Keywords**: Decisive-only<br/>
**Duration**: Instant<br/>
**Prerequisite Charms**: Charcoal March of Spiders Form

This Charm supplements a **decisive** attack, allowing the martial artist to gain 5 motes for each level of decisive damage dealt to his target. If the attack deals enough damage to kill the target outright, the martial artist also recovers 1 temporary Willpower.

## Pattern Spider Chicanery

**Cost**: 1wp; **Mins**: Martial Arts 5, Essence 5<br/>
**Type**: Reflexive<br/>
**Keywords**: Perilous, Sidereal-only<br/>
**Duration**: Instant<br/>
**Prerequisite Charms**: Cannibalistic Heritage Technique

The Loom of Fate is regularly pruned by the Pattern Spiders, as the dead ought to have no business affecting the future of Creation. If one is careful, though, this presents an opportunity.

The martial artist may reflexively activate this Charm after killing a target with an attack supplemented by Cannibalistic Heritage Technique. As the target’s Essence flows from their body, the martial artist inhales it briefly, then exhales it as new, reframing the parameters of their existence and reinserting them into the Loom of Fate.

The martial artist rolls (Manipulation + Martial Arts), and for each success they may alter one significant parameter of the target’s life when they reawaken. The target will understand these facts as inherently true, but may also retain conflicting memories and associations from their previous life.

This Charm negates any Essence or Willpower gained from Cannibalistic Heritage Technique, restoring any health taken from the target by that attack, but leaving them unconscious.

