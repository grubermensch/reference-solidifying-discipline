# Reference-Solidifying Discipline

**Cost**: 1wp; **Mins**: Homebrew 1, Essence 1<br/>
**Type**: Simple<br/>
**Keywords**: Written-only<br/>
**Duration**: Instant<br/>
**Prerequisite Charms**: Rust-Building Focus, Third-Edition-Fluency Approach

This Charm allows the Designer to define other Charms using a structured format. The Charm's
mechanical effects will be clearly and consistently specified in alignment with the rules of
Exalted Third Edition. To activate this Charm, the Designer supplies her Charmset in one or
more JSON files, and specifies the desired output format:

```bash
$: reference-solidifying-discipline --sources charms.json --format markdown --output charms.md
```