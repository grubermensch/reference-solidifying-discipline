use std::path::{Path, PathBuf};

mod helper;

fn examples_dir() -> PathBuf {
    Path::new(env!("CARGO_MANIFEST_DIR"))
        .join(Path::new("official-content"))
        .canonicalize()
        .unwrap()
}

fn name_from_subpath(subpath: &PathBuf) -> String {
    subpath
        .components()
        .map(|c| c.as_os_str().to_string_lossy().to_string())
        .collect::<Vec<String>>()
        .join("_")
}

macro_rules! test_generated_markdown {
    ($fn_name:ident, $path_string:expr) => {
        #[test]
        fn $fn_name() {
            let subpath = Path::new($path_string).to_path_buf();
            helper::test_generated_markdown(
                name_from_subpath(&subpath).as_str(),
                examples_dir().join(subpath),
            )
        }
    };
}

mod ex3 {
    use super::*;
    test_generated_markdown!(archery, "ex3/archery");
    test_generated_markdown!(athletics, "ex3/athletics");
    test_generated_markdown!(awareness, "ex3/awareness");
    test_generated_markdown!(brawl, "ex3/brawl");
}
