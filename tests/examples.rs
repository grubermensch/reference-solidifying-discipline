use std::path::{Path, PathBuf};

mod helper;

fn examples_dir() -> PathBuf {
    Path::new(env!("CARGO_MANIFEST_DIR"))
        .join(Path::new("examples"))
        .canonicalize()
        .unwrap()
}

fn name_from_subpath(subpath: &PathBuf) -> String {
    subpath
        .components()
        .map(|c| c.as_os_str().to_string_lossy().to_string())
        .collect::<Vec<String>>()
        .join("_")
}

macro_rules! test_generated_markdown {
    ($fn_name:ident, $path_string:expr) => {
        #[test]
        fn $fn_name() {
            let subpath = Path::new($path_string).to_path_buf();
            helper::test_generated_markdown(
                name_from_subpath(&subpath).as_str(),
                examples_dir().join(subpath),
            )
        }
    };
}

test_generated_markdown!(
    reference_solidifying_discipline,
    "reference-solidifying-discipline"
);
test_generated_markdown!(
    charcoal_march_of_spiders_homebrew,
    "charcoal-march-of-spiders-homebrew"
);
