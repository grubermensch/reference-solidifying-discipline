use std::path::{Path, PathBuf};
use std::process::Command;

pub fn binary_path() -> PathBuf {
    Path::new(env!("CARGO_BIN_EXE_reference-solidifying-discipline")).to_path_buf()
}

pub fn tmpdir(test_name: &str) -> PathBuf {
    Path::new(&std::env::temp_dir())
        .canonicalize()
        .unwrap()
        .join("integration-test-rsd")
        .join(Path::new(test_name))
        .to_path_buf()
}

pub fn run_rsd(test_name: &str, input_file: PathBuf) -> Result<PathBuf, String> {
    let tmpdir = tmpdir(test_name);
    if tmpdir.is_dir() {
        std::fs::remove_dir_all(&tmpdir).map_err(|e| e.to_string())?;
    }
    std::fs::create_dir_all(&tmpdir).map_err(|e| e.to_string())?;

    let output_file = tmpdir.join(Path::new("output.md"));
    let rsd_output = Command::new(binary_path())
        .args(&["--input", input_file.to_str().unwrap()])
        .args(&["--output", output_file.to_str().unwrap()])
        .output()
        .map_err(|e| e.to_string())?;
    if rsd_output.status.success() {
        Ok(output_file)
    } else {
        Err(format!(
            "Command execution failure: {}\nSTDOUT:\n{}\nSTDERR:\n{}",
            rsd_output.status,
            String::from_utf8_lossy(&rsd_output.stdout),
            String::from_utf8_lossy(&rsd_output.stderr),
        ))
    }
}

pub fn diff_files(expected: PathBuf, observed: PathBuf) -> Result<Option<String>, String> {
    let diff_output = Command::new("diff")
        .args(&[expected.to_str().unwrap()])
        .args(&[observed.to_str().unwrap()])
        .output()
        .map_err(|e| e.to_string())?;
    if diff_output.status.success() {
        Ok(None)
    } else {
        if diff_output.stderr.is_empty() {
            Ok(Some(
                String::from_utf8_lossy(&diff_output.stdout).to_string(),
            ))
        } else {
            Err(String::from_utf8_lossy(&diff_output.stderr).to_string())
        }
    }
}

pub fn test_generated_markdown(test_name: &str, directory: PathBuf) {
    let input_file = directory.join("input.json");
    let expected_output = directory.join("output.md");
    let result = run_rsd(test_name, input_file);
    match result {
        Ok(test_output) => {
            let diff = diff_files(expected_output, test_output);
            match diff {
                Ok(None) => (),
                Ok(Some(text)) => assert!(false, format!("Diffs to expected output:\n{}", text)),
                Err(e) => assert!(false, format!("Error: {}", e.to_string())),
            }
        }
        Err(e) => assert!(false, e.to_string()),
    }
}
